from flask import Flask, render_template, redirect, request
from flask_wtf.csrf import CSRFProtect
from urllib.parse import urlencode
from pymongo import MongoClient
from datetime import datetime

import requests
import hashlib
import json

app = Flask(__name__)
app.secret_key = "zvC9JmW6VnTtTeDCbAN4gGsoNao054"
csrf = CSRFProtect(app)

client = MongoClient("mongodb://admin:****@localhost:27017/")
database = client["ksolutions_test"]

def log_new_payment(**kargs) :
    kargs["date_time"] = datetime.utcnow()
    return database.work_log.insert_one(kargs).inserted_id

def get_payment_sign(required_data, secret_key="SecretKey01") :
    sign = "" 
    sorted_keys = sorted(required_data, key=str.lower)
    for key in sorted_keys :
        sign += str(required_data[key]) + ":"
    sign = sign[:-1] + secret_key
    
    return str(hashlib.sha256(sign.encode('utf-8')).hexdigest()) 

@app.route("/")
def index():
    return render_template('index.html', error=False)

@app.route("/payment", methods=["POST"])
def payment():
    amount = float(request.form.get('amount'))
    currency = request.form.get('currency')
    description = request.form.get('description')

    log_new_payment(amount=amount, 
                    currency=currency,
                    description=description)

    if currency == "EUR" :
        request_url = "https://pay.piastrix.com/ru/pay"
        request_data = {
            "amount": "{0:.2f}".format(amount),
            "currency": "978",
            "shop_id": "5",
            "shop_order_id": "101"
        }

        request_data["sign"] = get_payment_sign(request_data)
        request_data["description"] = description

        return redirect(request_url+"?"+urlencode(request_data))

    elif currency == "USD" :
        request_url = "https://core.piastrix.com/bill/create"
        request_data = {
            "shop_amount": "{0:.2f}".format(amount),
            "shop_currency": "840",
            "payer_currency": "840",
            "shop_id": "5",
            "shop_order_id": "101",
        }

        request_data["sign"] = get_payment_sign(request_data)
        request_data["description"] = description
        headers = {"Content-Type":"application/json"}

        piastrix_response = json.loads(requests.post(request_url, 
                                        data=json.dumps(request_data), 
                                        headers=headers).text)

        if piastrix_response["message"] == "Ok" and "data" in piastrix_response :
            return redirect(piastrix_response["data"]["url"])
        else :
            return render_template('index.html', 
                                    error=True, 
                                    error_text="Случилась неизвестная ошибка.")

    elif currency == "RUB" :
        request_url = "https://core.piastrix.com/invoice/create"
        request_data = {
            "amount": "{0:.2f}".format(amount),
            "currency": "643",
            "payway": "payeer_rub",
            "shop_id": "5",
            "shop_order_id": "101",
        }

        request_data["sign"] = get_payment_sign(request_data)
        request_data["description"] = description
        headers = {"Content-Type":"application/json"}

        piastrix_response = json.loads(requests.post(request_url, 
                                        data=json.dumps(request_data), 
                                        headers=headers).text)

        if piastrix_response["message"] == "Ok" :
            return redirect(piastrix_response["data"]["url"]+"?"+urlencode(piastrix_response["data"]["data"]))
        else :
            return render_template('index.html', 
                                    error=True, 
                                    error_text="Случилась неизвестная ошибка.")

if __name__ == "__main__":
    app.run(debug=True)
